# Create a list, called my_list, with three numbers. The total of the numbers added together should be 100.
my_list = [3, 2, 95]
# Create a tuple, called my_tuple, with a single value in it
my_tuple = (3,)
# Modify set2 so that set1.intersection(set2) returns {5, 77, 9, 12}
set1 = {14, 5, 9, 31, 12, 77, 67, 8}
set2 = {5, 77, 9, 12}
print(set1.intersection(set2))


Lista = [
    "p0xhvha9t2",
    "e6n387qhp5",
    "94g6yftgsj",
    "bx6mqr9vg2",
    "qjwaqevcez",
    "jh7y4a9kba",
    "xd6gpdqfch",
    "p4v5s82nc0",
]


lista_numeros = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    22,
    22,
    33,
    45,
    46,
]
# 1 lo que quiero en una nueva variable
# 2 ahi viene el for recorriendo lo necesario
# 3 se agrega el if luego de todo lo anterior
# 4 seria de esta manera [variable y operacion for variable in ]
List_Comprehenshion = [nums * 2 for nums in lista_numeros]
print(List_Comprehenshion)

*cabeza, cola = lista_numeros
print(cabeza, "\n", cola)
